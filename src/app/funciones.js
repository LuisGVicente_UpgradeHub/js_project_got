function createHeader(imgsrc, name, title) {
    var header = document.createElement('header');
    header.setAttribute('id', 'headerID');
    header.setAttribute('class', 'headerclass');

    var hearderimg = document.createElement('img');
    headerimg.setAttribute('id', 'headerimgID');
    headerimg.setAttribute('class', 'headerimgclass');
    headerimg.setAttribute('src', imgsrc);
    headerimg.setAttribute('alt', name);

    var h1title = document.createElement('h1');
    h1title.setAttribute('id', 'h1ID');
    h1title.setAttribute('class', h1class);
    var h1titleText = document.createTextNode(title);

    header.appendChild(hearderimg);
    header.appendChild(h1title);
    h1title.appendChild(h1titleText);

    return header;

}

// function createNav () {
//     var nav = document.createElement('nav');
//     nav.setAttribute('id', 'navID');
//     nav.setAttribute('class', 'navclass');

//     var ul =document.createElement('ul');
//     ul.setAttribute('id','ulID');
//     ul.setAttribute('class', 'ulclass');

//     var li= document.createElement('li');
//     li.setAttribute('id', 'liID');
//     li.setAttribute('class', 'liclass');

//     var link = document.createElement('a');
//     link.setAttribute('href', '#');

//     nav.appendChild(ul);
//     ul.appendChild(li);

//     return nav;

// }

function createSection (title, director, year, genre) {
    var section = document.createElement('section');
    section.setAttribute('id', 'sectID');
    section.setAttribute('class', 'sectclass');

    var title = document.createElement('div');
    title.setAttribute('id', 'titleID');
    title.setAttribute('class', 'titleclass');
    var h2tag = document.createElement('h2');
    var h2text = document.createTextNode(title);

    var director = document.createElement('div');
    director.setAttribute('id', 'directorID');
    director.setAttribute('class', 'directorclass');

    var h3tag = document.createElement('h3');
    var h3text = document.createTextNode(director);

    var year = document.createElement('div');
    year.setAttribute('id', 'yearID');
    year.setAttribute('class', 'yearclass');
    var h4year = document.createElement('h4');
    var h4yeartext = document.createTextNode(year);

    var genre = document.createElement('div');
    genre.setAttribute('id', 'genreID');
    genre.setAttribute('class', 'genreclass');
    var h4tag = document.createElement('h4');
    var h4text = document.createTextNode(genre);

    section.appendChild(title);
    section.appendChild(director);
    section.appendChild(year);
    section.appendChild(genre);
    title.appendChild(h2tag);
    h2tag.appendChild(h2text);
    director.appendChild(h3tag);
    h3tag.appendChild(h3text);
    year.appendChild(h4year);
    h4year.appendChild(h4yeartext);
    genre.appendChild(h4tag);
    h4tag.appendChild(h4text);

    return section;
}