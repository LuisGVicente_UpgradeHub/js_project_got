var gotBattles = [
    {
        "place": [
            "Dragonstone"
        ],
        "factionsOne": [
            "Greens"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "King Aegon II",
            "Targaryen"
        ],
        "commandersTwo": [
            "Queen Rhaenyra I Targaryen"
        ],
        "forcesOne": [
            "Dragonstone Garrison",
            "Aegon's Kingsguard",
            "Sunfyre"
        ],
        "forcesTwo": [
            "Rhaenyra's",
            "Queensguard"
        ],
        "casualties": [
            "None",
            "All of Rhaenyra's Queensguard",
            "Queen Rhaenyra I",
            "Prince Aegon",
            "Targaryen "
        ],
        "_id": "5cc074d404e71a0010b85ac4",
        "name": "Ambush at Dragonstone",
        "slug": "Ambush_at_Dragonstone",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.407Z",
        "updatedAt": "2019-04-24T14:38:12.407Z",
        "__v": 0
    },
    {
        "place": [
            "The North"
        ],
        "factionsOne": [
            "House Forrester",
            "Meereenese Warriors"
        ],
        "factionsTwo": [
            "House Whitehill"
        ],
        "commandersOne": [
            "Lord Rodrik",
            "Forrester",
            "Ser Royland Degore",
            "Duncan Tuttle",
            "Asher Forrester",
            "Amaya"
        ],
        "commandersTwo": [
            "Gryff Whitehill",
            "Harys"
        ],
        "forcesOne": [
            "~10 Forrester men-at-arms",
            "~20-25 pit fightersThe Beast &Bloodsong",
            "Beskha"
        ],
        "forcesTwo": [
            "~15 Whitehill men-at-arms~5 Warrick Crossbowmen"
        ],
        "casualties": [
            "Some Forrester men-at-armsRodrik Forrester Asher Forrester Some pit fightersThe Beast",
            "~10 Whitehill Men-at-arms"
        ],
        "_id": "5cc074d404e71a0010b85ac5",
        "name": "Ambush at the Harbor",
        "slug": "Ambush_at_the_Harbor",
        "conflict": "Forrester-Whitehill conflict",
        "createdAt": "2019-04-24T14:38:12.408Z",
        "updatedAt": "2019-04-24T14:38:12.408Z",
        "__v": 0
    },
    {
        "place": [
            "Tuttle Farm"
        ],
        "factionsOne": [
            "House Forrester"
        ],
        "factionsTwo": [
            "House Whitehill"
        ],
        "commandersOne": [
            "Lord Asher Forrester",
            "Ser Royland Degore",
            "Duncan Tuttle"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Small Group of Pit FightersAmaya"
        ],
        "forcesTwo": [
            "Small group of Whitehill soldiers"
        ],
        "casualties": [
            "At least one pit fighter",
            "Entire group of Whitehill soldiers killed"
        ],
        "_id": "5cc074d404e71a0010b85ac6",
        "name": "Ambush at the Tuttle Farm",
        "slug": "Ambush_at_the_Tuttle_Farm",
        "conflict": "Forrester-Whitehill conflict",
        "createdAt": "2019-04-24T14:38:12.408Z",
        "updatedAt": "2019-04-24T14:38:12.408Z",
        "__v": 0
    },
    {
        "place": [
            "Ashemark",
            " the Westerlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Bolton",
            "House Karstark"
        ],
        "factionsTwo": [
            "House Marbrand",
            "House Lorch"
        ],
        "commandersOne": [
            "King Robb Stark",
            "Lord Roose Bolton",
            "Lord Rickard Karstark"
        ],
        "commandersTwo": [
            "Lord Damon",
            "MarbrandSer Donnel Estren"
        ],
        "forcesOne": [
            "~2000 Karstark Cavalry~4000 Northern Cavalry",
            "~14,000 Rivermen/Northmen"
        ],
        "forcesTwo": [
            "~400 Marbrand Men",
            "~30 Lorch Cavalry",
            "~200 Estren Infantry"
        ],
        "casualties": [
            "~100",
            "~500 DeadMany Captured"
        ],
        "_id": "5cc074d404e71a0010b85ac7",
        "name": "Sack of Ashemark",
        "slug": "Sack_of_Ashemark",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.408Z",
        "updatedAt": "2019-04-24T14:38:12.408Z",
        "__v": 0
    },
    {
        "place": [
            "Ashford",
            " the Reach"
        ],
        "factionsOne": [
            "House Baratheon",
            "and Bannermen"
        ],
        "factionsTwo": [
            "House TarlyIn the vanguard",
            "House Tyrell",
            "Arrived after the",
            "battle was over"
        ],
        "commandersOne": [
            "Lord Robert Baratheon"
        ],
        "commandersTwo": [
            "Lord Randyll Tarly",
            "Lord Mace Tyrell"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85ac8",
        "name": "Battle of Ashford",
        "slug": "Battle_of_Ashford",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.408Z",
        "updatedAt": "2019-04-24T14:38:12.408Z",
        "__v": 0
    },
    {
        "place": [
            "Harrenhal"
        ],
        "factionsOne": [
            "Greens",
            "House Strong"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "Lord Larys Strong"
        ],
        "commandersTwo": [
            "Prince Daemon",
            "Targaryen"
        ],
        "forcesOne": [
            "Harrenhal garrison"
        ],
        "forcesTwo": [
            "Daemon Targaryen",
            "Caraxes"
        ],
        "casualties": [
            "None",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85ac9",
        "name": "Assault on Harrenhal",
        "slug": "Assault_on_Harrenhal",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.409Z",
        "updatedAt": "2019-04-24T14:38:12.409Z",
        "__v": 0
    },
    {
        "place": [
            "Oldtown"
        ],
        "factionsOne": [
            "House Dayne"
        ],
        "factionsTwo": [],
        "commandersOne": [
            "Ser Joffrey Dayne"
        ],
        "commandersTwo": [],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85aca",
        "name": "Assault on Oldtown",
        "slug": "Assault_on_Oldtown",
        "conflict": "First Dornish War",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.409Z",
        "updatedAt": "2019-04-24T14:38:12.409Z",
        "__v": 0
    },
    {
        "place": [
            "The Dreadfort",
            " the North"
        ],
        "factionsOne": [
            "House Bolton"
        ],
        "factionsTwo": [
            "House Greyjoy"
        ],
        "commandersOne": [
            "Ramsay Snow"
        ],
        "commandersTwo": [
            "Princess Yara Greyjoy"
        ],
        "forcesOne": [
            "Unknown number of Northmen",
            "Prince Theon Greyjoy"
        ],
        "forcesTwo": [
            "51 Ironborn raiders"
        ],
        "casualties": [
            "Light",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85acb",
        "name": "Assault on the Dreadfort",
        "slug": "Assault_on_the_Dreadfort",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.409Z",
        "updatedAt": "2019-04-24T14:38:12.409Z",
        "__v": 0
    },
    {
        "place": [
            "Blackwater Bay"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "House Greyjoy",
            "Dorne"
        ],
        "commandersOne": [
            "King Euron Greyjoy"
        ],
        "commandersTwo": [
            "Queen Yara Greyjoy ",
            "Prince Theon Greyjoy",
            "Captain Harrag",
            "Ellaria Sand "
        ],
        "forcesOne": [
            "Iron Fleet~1,000 ships,",
            "including SilenceIronborn warriors"
        ],
        "forcesTwo": [
            "Targaryen FleetAt least 100 ships in Yara's Iron Fleet,",
            "including Black WindIronborn warriors3 Sand Snakes of Dorne"
        ],
        "casualties": [
            "Moderate crew losses, light ship casualties",
            "Nearly absoluteObara SandNymeria SandAt least 97 ships sunk or captured"
        ],
        "_id": "5cc074d404e71a0010b85acc",
        "name": "Assault on the Targaryen Fleet",
        "slug": "Assault_on_the_Targaryen_Fleet",
        "conflict": "Daenerys Targaryen's invasion of Westeros",
        "createdAt": "2019-04-24T14:38:12.410Z",
        "updatedAt": "2019-04-24T14:38:12.410Z",
        "__v": 0
    },
    {
        "place": [
            "Astapor",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "House Targaryen"
        ],
        "factionsTwo": [
            "Astapor"
        ],
        "commandersOne": [
            "Queen Daenerys",
            "Targaryen",
            "Ser Jorah Mormont",
            "Ser Barristan Selmy"
        ],
        "commandersTwo": [
            "Good Masters &",
            "Kraznys mo",
            "Nakloz &",
            "Greizhen mo",
            "Ullhor &"
        ],
        "forcesOne": [
            "Over 8,000",
            "UnsulliedUnder 100 Khalasar3 Dragons"
        ],
        "forcesTwo": [
            "Astapori defenders, mostly household and civic guards"
        ],
        "casualties": [
            "Minimal, if any",
            "MassiveKraznys mo NaklozGreizhen mo UllhorMost, if not all Good Masters"
        ],
        "_id": "5cc074d404e71a0010b85acd",
        "name": "Sack of Astapor",
        "slug": "Sack_of_Astapor",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.410Z",
        "updatedAt": "2019-04-24T14:38:12.410Z",
        "__v": 0
    },
    {
        "place": [
            "Astapor",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "Yunkai"
        ],
        "factionsTwo": [
            "Empire of Astapor"
        ],
        "commandersOne": [
            "Wise Masters"
        ],
        "commandersTwo": [
            "Emperor Cleon"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85ace",
        "name": "Siege of Astapor",
        "slug": "Siege_of_Astapor",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.410Z",
        "updatedAt": "2019-04-24T14:38:12.410Z",
        "__v": 0
    },
    {
        "place": [
            "The skies above Harrenhal",
            " the Riverlands"
        ],
        "factionsOne": [
            "Greens"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "Prince Aemond",
            "Targaryen &"
        ],
        "commandersTwo": [
            "Prince Daemon",
            "Targaryen &"
        ],
        "forcesOne": [
            "Aemond on Vhagar"
        ],
        "forcesTwo": [
            "Daemon on Caraxes"
        ],
        "casualties": [
            "Aemond &",
            "Vhagar &",
            "Daemon &",
            "Caraxes &"
        ],
        "_id": "5cc074d404e71a0010b85acf",
        "name": "Battle Above the Gods Eye",
        "slug": "Battle_Above_the_Gods_Eye",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.411Z",
        "updatedAt": "2019-04-24T14:38:12.411Z",
        "__v": 0
    },
    {
        "place": [
            "The shores of Long Lake",
            " The North",
            " Westeros"
        ],
        "factionsOne": [
            "Free Folk"
        ],
        "factionsTwo": [
            "House Stark",
            "House Umber"
        ],
        "commandersOne": [
            "King Raymun",
            "Redbeard &"
        ],
        "commandersTwo": [
            "Lord Stark",
            "Lord Umber"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Heavy",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85ad0",
        "name": "Battle at Long Lake",
        "slug": "Battle_at_Long_Lake",
        "conflict": "",
        "createdAt": "2019-04-24T14:38:12.411Z",
        "updatedAt": "2019-04-24T14:38:12.411Z",
        "__v": 0
    },
    {
        "place": [
            "Cave of the Three-Eyed Raven",
            " Beyond the Wall"
        ],
        "factionsOne": [
            "House Stark",
            "Three-Eyed Raven",
            "Children of the Forest"
        ],
        "factionsTwo": [
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [
            "The Three-Eyed Raven &",
            "Brandon Stark"
        ],
        "commandersTwo": [
            "The Night King"
        ],
        "forcesOne": [
            "Three-Eyed Raven &",
            "6 of the Children of the Forest &Leaf &",
            "Bran Stark",
            "Meera Reed",
            "Hodor &",
            "Summer &",
            "Benjen Stark"
        ],
        "forcesTwo": [
            "The Night King",
            "3 White WalkersWight army"
        ],
        "casualties": [
            "Three-Eyed RavenAll Children of the ForestLeafHodorSummer",
            "One White WalkerHundreds of Wights"
        ],
        "_id": "5cc074d404e71a0010b85ad1",
        "name": "Battle at the Cave of the Three-Eyed Raven",
        "slug": "Battle_at_the_Cave_of_the_Three-Eyed_Raven",
        "conflict": "Conflict Beyond the Wall",
        "createdAt": "2019-04-24T14:38:12.411Z",
        "updatedAt": "2019-04-24T14:38:12.411Z",
        "__v": 0
    },
    {
        "place": [
            "Ironrath"
        ],
        "factionsOne": [
            "House Forrester",
            "Meereenese Warriors"
        ],
        "factionsTwo": [
            "House Whitehill"
        ],
        "commandersOne": [
            "Lord Rodrik Forrester",
            "Lord Asher Forrester",
            "Ser Royland Degore",
            "Duncan Tuttle",
            "Amaya &",
            "Beskha"
        ],
        "commandersTwo": [
            "Lord Ludd Whitehill",
            "Gryff Whitehill",
            "Harys &"
        ],
        "forcesOne": [
            "~30 men-at-arms~10 Archers",
            "~20 Pit Fighters"
        ],
        "forcesTwo": [
            "~450 Whitehills~40 Warricks"
        ],
        "casualties": [
            "Near AbsoluteMost Forrester men-at-arms killed or",
            "capturedBowen",
            "Erik",
            "Presumably all pit fightersAmayaBloodsong",
            "Ser Royland Degore",
            "Duncan Tuttle",
            "Lady Elissa ForresterOrtengryn",
            "ModerateMany Whitehill men-at-armsLord Ludd Whitehill",
            "Gryff Whitehill",
            "Harys"
        ],
        "_id": "5cc074d404e71a0010b85ad2",
        "name": "Battle of Ironrath",
        "slug": "Battle_of_Ironrath",
        "conflict": "Forrester-Whitehill conflict",
        "createdAt": "2019-04-24T14:38:12.411Z",
        "updatedAt": "2019-04-24T14:38:12.411Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell",
            " the North"
        ],
        "factionsOne": [
            "House Stark",
            "House Mormont",
            "House Hornwood",
            "House Mazin",
            "Free Folk",
            "House Arryn",
            "House Royce"
        ],
        "factionsTwo": [
            "House Bolton &",
            "House Karstark",
            "House Umber"
        ],
        "commandersOne": [
            "Jon Snow",
            "Chieftain Tormund",
            "Lady Lyanna",
            "Mormont",
            "Ser Davos Seaworth",
            "Lord Mazin",
            "Lady Sansa Stark",
            "Lord Petyr Baelish",
            "Lord Yohn Royce"
        ],
        "commandersTwo": [
            "Lord Ramsay",
            "Bolton",
            "Lord Harald",
            "Karstark &",
            "Lord Smalljon",
            "Umber &"
        ],
        "forcesOne": [
            "Around 2,400 men",
            "2,000 Free Folk",
            "warriors",
            "200 Hornwood",
            "soldiers",
            "143 Mazin soldiers",
            "62 Mormont soldiers",
            "1 giant",
            "Reinforcements:",
            "2,000 mounted knights of the Vale"
        ],
        "forcesTwo": [
            "Around 6,000 men",
            "5,000 Bolton",
            "soldiers1,000 soldiers",
            "contributions by House Karstark and House Umber"
        ],
        "casualties": [
            "Massive Stark-Wildling casualtiesRickon",
            "StarkWun Weg Wun Dar WunMinimal Arryn",
            "casualties",
            "Near total",
            "annihilation of the Bolton army; few",
            "survivors capturedLord Ramsay",
            "Bolton Lord Smalljon",
            "UmberLord Harald",
            "Karstark"
        ],
        "_id": "5cc074d404e71a0010b85ad3",
        "name": "Battle of the Bastards",
        "slug": "Battle_of_the_Bastards",
        "conflict": "War for the North",
        "createdAt": "2019-04-24T14:38:12.412Z",
        "updatedAt": "2019-04-24T14:38:12.412Z",
        "__v": 0
    },
    {
        "place": [
            "Fist of the First Men",
            " Beyond the Wall"
        ],
        "factionsOne": [
            "Night's Watch"
        ],
        "factionsTwo": [
            "White Walkers"
        ],
        "commandersOne": [
            "Lord Commander Jeor Mormont"
        ],
        "commandersTwo": [
            "White Walker",
            "Commander"
        ],
        "forcesOne": [
            "~300 men, including:  GrennEddison TollettKarl TannerRastBannen"
        ],
        "forcesTwo": [
            "White Walkers",
            "Hundreds of wights"
        ],
        "casualties": [
            "Over 200 men",
            "Numerous horses",
            "Unknown",
            "At least 1 wight"
        ],
        "_id": "5cc074d404e71a0010b85ad4",
        "name": "Battle of the Fist of the First Men",
        "slug": "Battle_of_the_Fist_of_the_First_Men",
        "conflict": "Conflict Beyond the Wall",
        "createdAt": "2019-04-24T14:38:12.412Z",
        "updatedAt": "2019-04-24T14:38:12.412Z",
        "__v": 0
    },
    {
        "place": [
            "Crossing of the Goldroad over the Blackwater Rush",
            " northern border of the Reach"
        ],
        "factionsOne": [
            "Iron Throne",
            "House Tarly"
        ],
        "factionsTwo": [
            "House Targaryen",
            "Dothraki"
        ],
        "commandersOne": [
            "Ser Jaime Lannister",
            "Ser Bronn",
            "Lord Randyll Tarly",
            "Dickon Tarly"
        ],
        "commandersTwo": [
            "Queen Daenerys",
            "Targaryen",
            "Lord Hand Tyrion Lannister",
            "Ko Qhono"
        ],
        "forcesOne": [
            "~10,000 Lannister",
            "Infantry and archers",
            "Unknown number of Tarly soldiers",
            "1 heavy anti-dragon scorpion"
        ],
        "forcesTwo": [
            "100,000 Dothraki",
            "light cavalry",
            "1 dragon "
        ],
        "casualties": [
            "HeavyRandyll Tarly",
            "Dickon Tarly",
            "Anti-dragon scorpion destroyedRemainder of the Lannister force bends the knee to Daenerys",
            "Mild",
            "Drogon mildly wounded"
        ],
        "_id": "5cc074d404e71a0010b85ad5",
        "name": "Battle of the Goldroad",
        "slug": "Battle_of_the_Goldroad",
        "conflict": "Daenerys Targaryen's invasion of Westeros",
        "createdAt": "2019-04-24T14:38:12.412Z",
        "updatedAt": "2019-04-24T14:38:12.412Z",
        "__v": 0
    },
    {
        "place": [
            "Gullet"
        ],
        "factionsOne": [
            "Greens",
            "Kingdom of the Three Daughters:LysMyrTyrosh"
        ],
        "factionsTwo": [
            "Blacks",
            "House Velaryon"
        ],
        "commandersOne": [
            "Unknown"
        ],
        "commandersTwo": [
            "Prince Jacaerys",
            "Velaryon &"
        ],
        "forcesOne": [
            "Unknown number of ships"
        ],
        "forcesTwo": [
            "3 Dragons",
            "Unknown number of ships"
        ],
        "casualties": [
            "Presumably moderate to low",
            "Prince Jacaerys",
            "Velaryon &",
            "Prince Aegon",
            "Targaryen ",
            "Prince Viserys",
            "Targaryen ",
            "2 dragons dead",
            "1 dragon badly",
            "injured",
            "Unknown number of ships sunk"
        ],
        "_id": "5cc074d404e71a0010b85ad6",
        "name": "Battle of the Gullet",
        "slug": "Battle_of_the_Gullet",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.412Z",
        "updatedAt": "2019-04-24T14:38:12.412Z",
        "__v": 0
    },
    {
        "place": [
            "The Redgrass Field",
            " southwest of King's Landing"
        ],
        "factionsOne": [
            "House Targaryen",
            "House Martell",
            "House Baratheon"
        ],
        "factionsTwo": [
            "House Blackfyre"
        ],
        "commandersOne": [
            "Lord Brynden Rivers",
            "Prince Baelor",
            "Targaryen",
            "Prince Maekar",
            "Targaryen"
        ],
        "commandersTwo": [
            "Daemon I",
            "Blackfyre &",
            "Lord Aegor Rivers"
        ],
        "forcesOne": [
            "Royalist Army",
            "Allied Dornish Army"
        ],
        "forcesTwo": [
            "Rebel Army"
        ],
        "casualties": [
            "HeavyThousands of",
            "Targaryen and",
            "Dornish soldiers",
            "HeavyThousands of rebel forcesDaemon Blackfyre and two of his sons"
        ],
        "_id": "5cc074d404e71a0010b85ad7",
        "name": "Battle of the Redgrass Field",
        "slug": "Battle_of_the_Redgrass_Field",
        "conflict": "Blackfyre Rebellion",
        "createdAt": "2019-04-24T14:38:12.413Z",
        "updatedAt": "2019-04-24T14:38:12.413Z",
        "__v": 0
    },
    {
        "place": [
            "Giant's Lance",
            " the Vale"
        ],
        "factionsOne": [
            "Andals:",
            "House Arryn"
        ],
        "factionsTwo": [
            "First Men:",
            "House Royce"
        ],
        "commandersOne": [
            "Ser Artys Arryn",
            "The Winged Knight"
        ],
        "commandersTwo": [
            "The Griffin King &",
            "King Robar II Royce &"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85ad8",
        "name": "Battle of the Seven Stars",
        "slug": "Battle_of_the_Seven_Stars",
        "conflict": "Andal Invasion",
        "createdAt": "2019-04-24T14:38:12.413Z",
        "updatedAt": "2019-04-24T14:38:12.413Z",
        "__v": 0
    },
    {
        "place": [
            "Stoney Sept",
            " The Riverlands"
        ],
        "factionsOne": [
            "House Baratheon",
            "House Stark",
            "House Arryn",
            "House Tully"
        ],
        "factionsTwo": [
            "Iron Throne"
        ],
        "commandersOne": [
            "Lord Robert Baratheon",
            "Lord Eddard Stark",
            "Lord Jon Arryn",
            "Lord Hoster Tully"
        ],
        "commandersTwo": [
            "Lord Hand Jon",
            "Connington",
            "Ser Jonothor Darry"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown,",
            "heavier than the rebels"
        ],
        "_id": "5cc074d404e71a0010b85ad9",
        "name": "Battle of the Bells",
        "slug": "Battle_of_the_Bells",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.413Z",
        "updatedAt": "2019-04-24T14:38:12.413Z",
        "__v": 0
    },
    {
        "place": [
            "King's Landing and the mouth of the Blackwater Rush",
            " theCrownlands"
        ],
        "factionsOne": [
            "Iron Throne",
            "Kingsguard",
            "City Watch of King's Landing",
            "Crownlands",
            "Bannermen",
            "House Stokeworth",
            "House Lannister",
            "Tywin Lannister's Host",
            "House Tyrell"
        ],
        "factionsTwo": [
            "House Baratheon of Dragonstone",
            "Houses of the Narrow Sea",
            "House Velaryon",
            "Houses of The",
            "Stormlands",
            "House Seaworth",
            "House Florent",
            "Lysene Sellsails"
        ],
        "commandersOne": [
            "King's Landing:",
            "King Joffrey I Baratheon",
            "Lord Hand Tyrion",
            "Lannister",
            "Commander Bronn",
            "Wisdom Hallyne",
            "Relief Force:",
            "Lord Tywin Lannister",
            "Lord Mace Tyrell",
            "Ser Loras Tyrell"
        ],
        "commandersTwo": [
            "King Stannis I Baratheon",
            "Ser",
            "Davos Seaworth",
            "First Mate Matthos Seaworth &",
            "Ser Imry Florent",
            "Captain Salladhor Saan"
        ],
        "forcesOne": [
            "2,000 men of the City Watch",
            "Unknown number of soldiers of the House Lannister",
            "20 ships of the Royal Navy",
            "Unknown number of Lannister reinforcements and at least 15,800 of House Tyrell"
        ],
        "forcesTwo": [
            "More than 20,000 men",
            "~200 ships, including 30 Lysene sellsailsFury",
            "Black Betha"
        ],
        "casualties": [
            "Heavy",
            "Many gold cloaks deserted",
            "Ser Mandon Moore",
            "At least 16,000 killed or wounded.At least 138 ships &quot;Black Betha&quot;Matthos Seaworth"
        ],
        "_id": "5cc074d404e71a0010b85ada",
        "name": "Battle of the Blackwater",
        "slug": "Battle_of_the_Blackwater",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.413Z",
        "updatedAt": "2019-04-24T14:38:12.413Z",
        "__v": 0
    },
    {
        "place": [
            "Eastwatch-by-the-Sea",
            " the Wall",
            " the Gift"
        ],
        "factionsOne": [
            "Night's Watch",
            "Free Folk",
            "Brotherhood Without Banners"
        ],
        "factionsTwo": [
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [],
        "commandersTwo": [],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "HeavyEastwatch castle destroyed",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85adb",
        "name": "Breaching of the Wall",
        "slug": "Breaching_of_the_Wall",
        "conflict": "Conflict Beyond the Wall / Great War",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.414Z",
        "updatedAt": "2019-04-24T14:38:12.414Z",
        "__v": 0
    },
    {
        "place": [
            "Harrenhal",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Targaryen",
            "The Riverlords led by House Tully"
        ],
        "factionsTwo": [
            "The Isles and Rivers"
        ],
        "commandersOne": [
            "King Aegon I",
            "Targaryen",
            "Lord Edmyn Tully"
        ],
        "commandersTwo": [
            "King Harren Hoare &"
        ],
        "forcesOne": [
            "Uncertain",
            "1 dragon "
        ],
        "forcesTwo": [
            "Uncertain",
            "Harrenhal Garrison"
        ],
        "casualties": [
            "Minimal",
            "Heavy",
            "King Harren and all his sons;",
            "Castle populace",
            "exterminated"
        ],
        "_id": "5cc074d404e71a0010b85adc",
        "name": "Burning of Harrenhal",
        "slug": "Burning_of_Harrenhal",
        "conflict": "War of Conquest",
        "createdAt": "2019-04-24T14:38:12.414Z",
        "updatedAt": "2019-04-24T14:38:12.414Z",
        "__v": 0
    },
    {
        "place": [
            "Iron Islands"
        ],
        "factionsOne": [
            "House Lannister"
        ],
        "factionsTwo": [
            "House Greyjoy"
        ],
        "commandersOne": [
            "Unknown"
        ],
        "commandersTwo": [
            "Unknown vassals of House Greyjoy"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Low",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85add",
        "name": "Burning of the Iron Islands",
        "slug": "Burning_of_the_Iron_Islands",
        "conflict": "Spillover of the Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.414Z",
        "updatedAt": "2019-04-24T14:38:12.414Z",
        "__v": 0
    },
    {
        "place": [
            "Sept of Remembrance",
            " King's Landing",
            " Westeros"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "Faith of the Seven"
        ],
        "commandersOne": [
            "King Maegor I Targaryen"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "1 Dragon ",
            "Unknown amount of soldiers"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "None",
            "Complete destruction"
        ],
        "_id": "5cc074d404e71a0010b85ade",
        "name": "Burning of the Sept of Remembrance",
        "slug": "Burning_of_the_Sept_of_Remembrance",
        "conflict": "Faith Militant uprising",
        "createdAt": "2019-04-24T14:38:12.414Z",
        "updatedAt": "2019-04-24T14:38:12.414Z",
        "__v": 0
    },
    {
        "place": [
            "Near of the Gods Eye"
        ],
        "factionsOne": [
            "Greens",
            "Kingsguard"
        ],
        "factionsTwo": [
            "Blacks",
            "Riverlords"
        ],
        "commandersOne": [
            "Lord Hand Criston Cole &"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Heavy",
            "Light"
        ],
        "_id": "5cc074d404e71a0010b85adf",
        "name": "Butcher's Ball",
        "slug": "Butcher%27s_Ball",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.415Z",
        "updatedAt": "2019-04-24T14:38:12.415Z",
        "__v": 0
    },
    {
        "place": [
            "Riverrun",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Karstark",
            "House Umber",
            "House Frey"
        ],
        "factionsTwo": [
            "House Lannister",
            "House Marbrand",
            "House Lorch",
            "House Westerling"
        ],
        "commandersOne": [
            "Lord Robb Stark",
            "Lord Rickard Karstark",
            "Lord Greatjon Umber"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Low",
            "High"
        ],
        "_id": "5cc074d404e71a0010b85ae0",
        "name": "Battle of the Camps",
        "slug": "Battle_of_the_Camps",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.415Z",
        "updatedAt": "2019-04-24T14:38:12.415Z",
        "__v": 0
    },
    {
        "place": [
            "Riverrun",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Tully"
        ],
        "factionsTwo": [
            "House Frey"
        ],
        "commandersOne": [
            "Ser Brynden Tully"
        ],
        "commandersTwo": [
            "Lothar Frey",
            "Walder Rivers"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Light, if any",
            "Total"
        ],
        "_id": "5cc074d404e71a0010b85ae1",
        "name": "Capture of Riverrun",
        "slug": "Capture_of_Riverrun",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.415Z",
        "updatedAt": "2019-04-24T14:38:12.415Z",
        "__v": 0
    },
    {
        "place": [
            "Castle Black",
            " the Wall"
        ],
        "factionsOne": [
            "Night's Watch",
            "House Baratheon of Dragonstone"
        ],
        "factionsTwo": [
            "Free Folk"
        ],
        "commandersOne": [
            "Ser Alliser Thorne ",
            "Janos Slynt",
            "Jon Snow",
            "Eddison Tollett",
            "King Stannis Baratheon",
            "Ser",
            "Davos Seaworth"
        ],
        "commandersTwo": [
            "King Mance Rayder",
            "Chieftain Tormund ",
            "Magnar Styr &",
            "King Mag Mar Tun Doh Weg &"
        ],
        "forcesOne": [
            "Day 1: ~1001 direwolf ",
            "Day 2: ~2,800 Baratheon cavalry"
        ],
        "forcesTwo": [
            "~100,000 wildlingsTormund's warbandStyr's warband2 giants1 mammoth"
        ],
        "casualties": [
            "50 men of the Night's WatchPypar &Grenn &Hill &Cooper &Minimal Baratheon casualties",
            "Heavy Entire southern",
            "assault force",
            "eliminated",
            "Styr &Thenn warg &Ygritte &Dongo &Mag Mar Tun Doh Weg &Main force killed, scattered, or captured "
        ],
        "_id": "5cc074d404e71a0010b85ae2",
        "name": "Battle of Castle Black",
        "slug": "Battle_of_Castle_Black",
        "conflict": "Conflict Beyond the Wall / War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.416Z",
        "updatedAt": "2019-04-24T14:38:12.416Z",
        "__v": 0
    },
    {
        "place": [
            "Craster's Keep",
            " the Haunted Forest",
            " Beyond the Wall"
        ],
        "factionsOne": [
            "Night's Watch",
            "House Bolton",
            " House Stark"
        ],
        "factionsTwo": [
            "Mutineers"
        ],
        "commandersOne": [
            "Jon SnowPrince Bran Stark"
        ],
        "commandersTwo": [
            "Karl Tanner &"
        ],
        "forcesOne": [
            "Grenn",
            "Eddison Tollett",
            "Locke &",
            "Ghost",
            "Other black brothers",
            "Jojen ReedMeera ReedHodor"
        ],
        "forcesTwo": [
            "11 Mutinous black brothersRast &"
        ],
        "casualties": [
            "Five black brothersLocke",
            "All mutineers killed"
        ],
        "_id": "5cc074d404e71a0010b85ae3",
        "name": "Raid on Craster's Keep",
        "slug": "Raid_on_Craster%27s_Keep",
        "conflict": "Conflict Beyond the WallWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.416Z",
        "updatedAt": "2019-04-24T14:38:12.416Z",
        "__v": 0
    },
    {
        "place": [
            "Above Storm's End",
            " the Stormlands"
        ],
        "factionsOne": [
            "Greens"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [],
        "commandersTwo": [],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "None",
            "Lucerys VelaryonArrax"
        ],
        "_id": "5cc074d404e71a0010b85ae4",
        "name": "Dance Over Storm's End",
        "slug": "Dance_Over_Storm%27s_End",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.416Z",
        "updatedAt": "2019-04-24T14:38:12.416Z",
        "__v": 0
    },
    {
        "place": [
            "Hellholt"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "Dorne",
            "House Uller"
        ],
        "commandersOne": [
            "Queen Rhaenys",
            "Targaryen &"
        ],
        "commandersTwo": [
            "Lord Uller"
        ],
        "forcesOne": [
            "1 dragonMeraxes"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Meraxes",
            "Rhaenys Targaryen",
            "Many Uller infantry"
        ],
        "_id": "5cc074d404e71a0010b85ae5",
        "name": "Death of Meraxes",
        "slug": "Death_of_Meraxes",
        "conflict": "First Dornish War",
        "createdAt": "2019-04-24T14:38:12.416Z",
        "updatedAt": "2019-04-24T14:38:12.416Z",
        "__v": 0
    },
    {
        "place": [
            "Deepwood Motte",
            " the North"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "House Glover"
        ],
        "commandersOne": [
            "Princess Yara Greyjoy"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "At least 500 men"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Civilian casualties"
        ],
        "_id": "5cc074d404e71a0010b85ae6",
        "name": "First Battle of Deepwood Motte",
        "slug": "First_Battle_of_Deepwood_Motte",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.417Z",
        "updatedAt": "2019-04-24T14:38:12.417Z",
        "__v": 0
    },
    {
        "place": [
            "Tarbeck Hall"
        ],
        "factionsOne": [
            "House Lannister"
        ],
        "factionsTwo": [
            "House Tarbeck &",
            "House Reyne"
        ],
        "commandersOne": [
            "Ser Tywin Lannister",
            "Ser Kevan Lannister"
        ],
        "commandersTwo": [
            "Lady Ellyn Tarbeck &",
            "Lord Roger Reyne "
        ],
        "forcesOne": [
            "13,000 Men From Westerlands Houses500 Knights",
            "3,000 Crossbowmen"
        ],
        "forcesTwo": [
            "2,000 Men of House Reyne400 Men From House Tarbeck",
            "500 Knights"
        ],
        "casualties": [
            "300 Killed659 Injured",
            "Complete (in the",
            "castle)",
            "Heavy (on the",
            "battlefield)Tion TarbeckEllyn Tarbeck"
        ],
        "_id": "5cc074d404e71a0010b85ae7",
        "name": "Destruction of Tarbeck Hall",
        "slug": "Destruction_of_Tarbeck_Hall",
        "conflict": "Reyne Rebellion",
        "createdAt": "2019-04-24T14:38:12.417Z",
        "updatedAt": "2019-04-24T14:38:12.417Z",
        "__v": 0
    },
    {
        "place": [
            "Highgarden"
        ],
        "factionsOne": [
            "Dornish forces"
        ],
        "factionsTwo": [
            "The Reach"
        ],
        "commandersOne": [],
        "commandersTwo": [],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85ae8",
        "name": "Dornish Sack of Highgarden",
        "slug": "Dornish_Sack_of_Highgarden",
        "conflict": "",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.417Z",
        "updatedAt": "2019-04-24T14:38:12.417Z",
        "__v": 0
    },
    {
        "place": [
            "Dorne"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "Dorne"
        ],
        "commandersOne": [
            "King Aegon I",
            "Targaryen",
            "Queen Visenya",
            "Targaryen"
        ],
        "commandersTwo": [
            "Princess Meria Martell"
        ],
        "forcesOne": [
            "Targaryen Army",
            "2 DragonsBalerionVhagar"
        ],
        "forcesTwo": [
            "Dorne Army"
        ],
        "casualties": [
            "Moderate",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85ae9",
        "name": "Dragon's Wroth",
        "slug": "Dragon%27s_Wroth",
        "conflict": "First Dornish War",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.417Z",
        "updatedAt": "2019-04-24T14:38:12.417Z",
        "__v": 0
    },
    {
        "place": [
            "Dragonstone",
            " Blackwater Bay"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "House Targaryen"
        ],
        "commandersOne": [
            "Ser Stannis Baratheon"
        ],
        "commandersTwo": [
            "Ser Willem Darry"
        ],
        "forcesOne": [
            "Royal FleetFury"
        ],
        "forcesTwo": [
            "Dragonstone",
            "garrison"
        ],
        "casualties": [
            "Light",
            "Light",
            "Garrison surrendered"
        ],
        "_id": "5cc074d404e71a0010b85aea",
        "name": "Assault on Dragonstone",
        "slug": "Assault_on_Dragonstone",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.418Z",
        "updatedAt": "2019-04-24T14:38:12.418Z",
        "__v": 0
    },
    {
        "place": [
            "Braavos",
            " the Free Cities"
        ],
        "factionsOne": [
            "Faceless Men"
        ],
        "factionsTwo": [
            "Arya Stark"
        ],
        "commandersOne": [
            "Jaqen H'ghar"
        ],
        "commandersTwo": [
            "Arya Stark"
        ],
        "forcesOne": [
            "The Waif &"
        ],
        "forcesTwo": [
            "Arya Stark",
            "Lady Crane &"
        ],
        "casualties": [
            "The Waif",
            "Lady Crane"
        ],
        "_id": "5cc074d404e71a0010b85aeb",
        "name": "Duel in Braavos",
        "slug": "Duel_in_Braavos",
        "conflict": "",
        "createdAt": "2019-04-24T14:38:12.418Z",
        "updatedAt": "2019-04-24T14:38:12.418Z",
        "__v": 0
    },
    {
        "place": [
            "Fair Isle",
            " the Westerlands"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "House Baratheon of Dragonstone"
        ],
        "commandersOne": [
            "Captain Euron Greyjoy",
            "Captain Aeron Greyjoy"
        ],
        "commandersTwo": [
            "Lord Stannis Baratheon"
        ],
        "forcesOne": [
            "Iron Fleet"
        ],
        "forcesTwo": [
            "Royal Fleet"
        ],
        "casualties": [
            "HeavyIron Fleet",
            "Light"
        ],
        "_id": "5cc074d404e71a0010b85aec",
        "name": "Battle of Fair Isle",
        "slug": "Battle_of_Fair_Isle",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.418Z",
        "updatedAt": "2019-04-24T14:38:12.418Z",
        "__v": 0
    },
    {
        "place": [
            "Red Keep",
            " King's Landing",
            " Westeros"
        ],
        "factionsOne": [
            "House Targaryen",
            "Kingsguard"
        ],
        "factionsTwo": [
            "Faith Militant"
        ],
        "commandersOne": [
            "Ser Raymont Baratheon &"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Kingsguard",
            "Castle Garrison"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85aed",
        "name": "Faith Militant raid on the Red Keep",
        "slug": "Faith_Militant_raid_on_the_Red_Keep",
        "conflict": "Faith Militant uprising",
        "createdAt": "2019-04-24T14:38:12.418Z",
        "updatedAt": "2019-04-24T14:38:12.418Z",
        "__v": 0
    },
    {
        "place": [
            "Casterly Rock"
        ],
        "factionsOne": [
            "Iron Throne",
            "House Greyjoy"
        ],
        "factionsTwo": [
            "House Targaryen",
            "Unsullied"
        ],
        "commandersOne": [
            "Ser Jaime Lannister",
            "King Euron Greyjoy"
        ],
        "commandersTwo": [
            "Lord Hand Tyrion Lannister",
            "Commander Grey Worm"
        ],
        "forcesOne": [
            "Casterly Rock garrison ",
            "Iron Fleet :Silence"
        ],
        "forcesTwo": [
            "8,000+ Unsullied heavy infantry",
            "80 ships from the",
            "Small amount of the Targaryen fleet"
        ],
        "casualties": [
            "All Lannister Soldiers",
            "All ships",
            "~150 Unsullied at most"
        ],
        "_id": "5cc074d404e71a0010b85aee",
        "name": "Fall of Casterly Rock",
        "slug": "Fall_of_Casterly_Rock",
        "conflict": "Daenerys Targaryen's invasion of Westeros",
        "createdAt": "2019-04-24T14:38:12.419Z",
        "updatedAt": "2019-04-24T14:38:12.419Z",
        "__v": 0
    },
    {
        "place": [
            "King's Landing"
        ],
        "factionsOne": [
            "Greens",
            "City Watch"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "Green Council"
        ],
        "commandersTwo": [
            "Queen Rhaenyra",
            "Targaryen",
            "Prince Daemon",
            "Targaryen"
        ],
        "forcesOne": [
            "City Watch"
        ],
        "forcesTwo": [
            "Two dragons"
        ],
        "casualties": [
            "Queens Helaena Targaryen and Alicent Hightower taken hostage",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85aef",
        "name": "Fall of King's Landing",
        "slug": "Fall_of_King%27s_Landing",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.419Z",
        "updatedAt": "2019-04-24T14:38:12.419Z",
        "__v": 0
    },
    {
        "place": [
            "Last Hearth",
            " the North"
        ],
        "factionsOne": [
            "House Umber &"
        ],
        "factionsTwo": [
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [
            "Lord Ned Umber &"
        ],
        "commandersTwo": [
            "Night King"
        ],
        "forcesOne": [
            "Unknown number of Umber troops"
        ],
        "forcesTwo": [
            "~100,000 wights of the army of the dead"
        ],
        "casualties": [
            "TotalNed Umber",
            "Unknown; most likely minimal, if any"
        ],
        "_id": "5cc074d404e71a0010b85af0",
        "name": "Fall of Last Hearth",
        "slug": "Fall_of_Last_Hearth",
        "conflict": "Great War",
        "createdAt": "2019-04-24T14:38:12.419Z",
        "updatedAt": "2019-04-24T14:38:12.419Z",
        "__v": 0
    },
    {
        "place": [
            "Moat Cailin",
            " the North"
        ],
        "factionsOne": [
            "House Greyjoy"
        ],
        "factionsTwo": [
            "House Stark"
        ],
        "commandersOne": [
            "Ralf KenningAdrack Humble"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Unknown",
            "Garrison destroyed or surrendered"
        ],
        "_id": "5cc074d404e71a0010b85af1",
        "name": "Fall of Moat Cailin",
        "slug": "Fall_of_Moat_Cailin",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.419Z",
        "updatedAt": "2019-04-24T14:38:12.419Z",
        "__v": 0
    },
    {
        "place": [
            "Sarne",
            " Kingdom of Sarnor",
            " Essos"
        ],
        "factionsOne": [
            "Dothraki"
        ],
        "factionsTwo": [
            "Kingdom of Sarnor &"
        ],
        "commandersOne": [
            "Khal Haro &",
            "Khal Qano",
            "Khal Loso",
            "Khal Zhako"
        ],
        "commandersTwo": [
            "High King Mazor Alexi &"
        ],
        "forcesOne": [
            "80,000 riders"
        ],
        "forcesTwo": [
            "100,000 foot soldiers",
            "10,000 heavy cavalry",
            "10,000 light cavalry",
            "6,000 scythed chariots"
        ],
        "casualties": [
            "Heavy",
            "Complete host destroyed",
            "High King Mazor Alexi"
        ],
        "_id": "5cc074d404e71a0010b85af2",
        "name": "Field of Crows",
        "slug": "Field_of_Crows",
        "conflict": "Century of Blood",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.420Z",
        "updatedAt": "2019-04-24T14:38:12.420Z",
        "__v": 0
    },
    {
        "place": [
            "The northern Reach"
        ],
        "factionsOne": [
            "House Targaryen"
        ],
        "factionsTwo": [
            "Iron Fist",
            "The Rock",
            "The Reach"
        ],
        "commandersOne": [
            "King Aegon I",
            "Targaryen",
            "Queen Visenya",
            "Targaryen",
            "Queen Rhaenys",
            "Targaryen"
        ],
        "commandersTwo": [
            "King Mern IX",
            "Gardener &",
            "King Loren I",
            "Lannister"
        ],
        "forcesOne": [
            "~10,000 men",
            "3 dragonsBalerionMeraxesVhagar"
        ],
        "forcesTwo": [
            "Nearly 60,000 men, including 5,000 knights and 55,000 men at arms"
        ],
        "casualties": [
            "Unknown",
            "5,000 killedTens of thousands woundedKing Mern IXEntire male line of House Gardener wiped out"
        ],
        "_id": "5cc074d404e71a0010b85af3",
        "name": "Field of Fire",
        "slug": "Field_of_Fire",
        "conflict": "War of Conquest",
        "createdAt": "2019-04-24T14:38:12.420Z",
        "updatedAt": "2019-04-24T14:38:12.420Z",
        "__v": 0
    },
    {
        "place": [
            ""
        ],
        "factionsOne": [],
        "factionsTwo": [],
        "commandersOne": [],
        "commandersTwo": [],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85af4",
        "name": "The Field of Fire",
        "slug": "The_Field_of_Fire",
        "conflict": "",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.420Z",
        "updatedAt": "2019-04-24T14:38:12.420Z",
        "__v": 0
    },
    {
        "place": [
            "Unidenfied holdfast by the Gods Eye",
            " the Riverlands"
        ],
        "factionsOne": [
            "Night's Watch"
        ],
        "factionsTwo": [
            "House Lannister",
            "City Watch"
        ],
        "commandersOne": [
            "Recruiter Yoren &"
        ],
        "commandersTwo": [
            "Ser Amory Lorch"
        ],
        "forcesOne": [
            "Night's Watch recruitsGendry Arya Stark Hot Pie "
        ],
        "forcesTwo": [
            "Lannister men-at-armsPolliverGoldcloaks"
        ],
        "casualties": [
            "LightYorenLommySurvivors captured",
            "Minimal; several Lannister men killed"
        ],
        "_id": "5cc074d404e71a0010b85af5",
        "name": "Raid by the Gods Eye",
        "slug": "Raid_by_the_Gods_Eye",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.420Z",
        "updatedAt": "2019-04-24T14:38:12.420Z",
        "__v": 0
    },
    {
        "place": [
            "Golden Tooth"
        ],
        "factionsOne": [
            "House Lannister",
            "Unnamed Lannister Bannermen"
        ],
        "factionsTwo": [
            "House Tully",
            "Unnamed Tully",
            "Bannermen"
        ],
        "commandersOne": [
            "Ser Jaime Lannister"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "30,000 men"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85af6",
        "name": "Battle of the Golden Tooth",
        "slug": "Battle_of_the_Golden_Tooth",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.421Z",
        "updatedAt": "2019-04-24T14:38:12.421Z",
        "__v": 0
    },
    {
        "place": [
            "The Gift"
        ],
        "factionsOne": [
            "Free Folk"
        ],
        "factionsTwo": [
            "House Stark",
            "Night's Watch"
        ],
        "commandersOne": [
            "King-Beyond-the-Wall Gorne",
            "King-Beyond-the-Wall Gendel"
        ],
        "commandersTwo": [
            "King in the North",
            "Lord Commander of the Night's Watch"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85af7",
        "name": "Gorne and Gendel's War",
        "slug": "Gorne_and_Gendel%27s_War",
        "conflict": "",
        "createdAt": "2019-04-24T14:38:12.421Z",
        "updatedAt": "2019-04-24T14:38:12.421Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell",
            " the North"
        ],
        "factionsOne": [
            "House Stark",
            "House Karstark",
            "House Mormont",
            "Other bannermen of House Stark ",
            "House Targaryen",
            "Dothraki",
            "Unsullied",
            "House Arryn",
            "House Royce",
            "Other bannermen of House Arryn ",
            "House Greyjoy",
            "Night's Watch",
            "Free Folk",
            "Brotherhood Without Banners"
        ],
        "factionsTwo": [
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [
            "Queen Daenerys",
            "Targaryen",
            "Warden Jon Snow",
            "Lady Sansa Stark",
            "Prince Theon Greyjoy",
            "Ser Jorah Mormont",
            "Grey Worm",
            "Ser Jaime Lannister",
            "Lord Beric Dondarrion",
            "Chieftain Tormund",
            "Giantsbane",
            "Lady Lyanna",
            "Mormont",
            "Lady Alys Karstark",
            "Lord Yohn Royce",
            "Lord Commander Eddison Tollett",
            "Ser Brienne of Tarth"
        ],
        "commandersTwo": [
            "Night King"
        ],
        "forcesOne": [
            "Stark infantry and archers",
            "1 direwolf",
            "~8,000 Unsullied heavy infantry",
            "~100,000 Dothraki light cavalry",
            "2 dragons ",
            "~2,000 knights of the Vale",
            "Ironborn sailors and warriors loyal to Yara and Theon"
        ],
        "forcesTwo": [
            "White Walkers",
            "~100,000 wights",
            "3 undead giants ",
            "1 undead dragon "
        ],
        "casualties": [],
        "_id": "5cc074d404e71a0010b85af8",
        "name": "Great Battle of Winterfell",
        "slug": "Great_Battle_of_Winterfell",
        "conflict": "Great War",
        "createdAt": "2019-04-24T14:38:12.421Z",
        "updatedAt": "2019-04-24T14:38:12.421Z",
        "__v": 0
    },
    {
        "place": [
            "Great Wyk",
            " the Iron Islands"
        ],
        "factionsOne": [
            "House Baratheon of Dragonstone"
        ],
        "factionsTwo": [
            "Salt Throne"
        ],
        "commandersOne": [
            "Lord Stannis Baratheon"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85af9",
        "name": "Siege of Great Wyk",
        "slug": "Siege_of_Great_Wyk",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.422Z",
        "updatedAt": "2019-04-24T14:38:12.422Z",
        "__v": 0
    },
    {
        "place": [
            "Along the western side of the Green Fork in the Riverlands"
        ],
        "factionsOne": [
            "House Lannister",
            "Lannister Vassal Houses",
            "Hill Tribes of The ValeStone CrowsBurned MenBlack EarsMoon BrothersPainted Dogs"
        ],
        "factionsTwo": [
            "House Stark",
            "Stark Vassal Houses"
        ],
        "commandersOne": [
            "Lord Tywin Lannister",
            "Ser Kevan Lannister",
            "Ser Addam Marbrand",
            "Lord Leo Lefford",
            "Tyrion Lannister"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "30,000 Lannister",
            "soldiersAn unknown number of hill tribesmen"
        ],
        "forcesTwo": [
            "2,000 Northern",
            "soldiers"
        ],
        "casualties": [
            "Light; a couple hundred or less",
            "(Mostly Hill",
            "Tribesmen)",
            "Army broken, 2,000 killed or captured"
        ],
        "_id": "5cc074d404e71a0010b85afa",
        "name": "Battle of the Green Fork",
        "slug": "Battle_of_the_Green_Fork",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.422Z",
        "updatedAt": "2019-04-24T14:38:12.422Z",
        "__v": 0
    },
    {
        "place": [
            "King's Landing",
            " the Crownlands."
        ],
        "factionsOne": [
            "House Lannister",
            "House Clegane"
        ],
        "factionsTwo": [
            "Iron Throne",
            "Alchemists' Guild"
        ],
        "commandersOne": [
            "Lord Tywin Lannister",
            "Ser Gregor Clegane"
        ],
        "commandersTwo": [
            "King Aerys II",
            "Targaryen &",
            "Lord Hand Wisdom Rossart &"
        ],
        "forcesOne": [
            "10,000",
            "Lannister soldiers"
        ],
        "forcesTwo": [
            "Small number of",
            "defenders"
        ],
        "casualties": [
            "Light, if any",
            "MassiveKing Aerys II",
            "Targaryen &Princess Elia Martell &Prince Aegon",
            "Targaryen &Princess Rhaenys Targaryen &Wisdom Rossart &"
        ],
        "_id": "5cc074d404e71a0010b85afb",
        "name": "Sack of King's Landing",
        "slug": "Sack_of_King%27s_Landing",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.422Z",
        "updatedAt": "2019-04-24T14:38:12.422Z",
        "__v": 0
    },
    {
        "place": [
            "Storm's End",
            " the Stormlands"
        ],
        "factionsOne": [
            "House Targaryen"
        ],
        "factionsTwo": [
            "The Stormlands"
        ],
        "commandersOne": [
            "Lord Hand  Orys Baratheon",
            "Queen Rhaenys",
            "Targaryen"
        ],
        "commandersTwo": [
            "King Argilac",
            "Durrandon &",
            "Queen Argella",
            "Durrandon "
        ],
        "forcesOne": [
            "2,500 spearmen500 Rivermen cavalry 1 dragon"
        ],
        "forcesTwo": [
            "1,500 cavalry4,500 infantry/archers"
        ],
        "casualties": [
            "~1,000 dead or wounded",
            "~2,800 deadKing Argilac"
        ],
        "_id": "5cc074d404e71a0010b85afc",
        "name": "Last Storm",
        "slug": "Last_Storm",
        "conflict": "War of Conquest",
        "createdAt": "2019-04-24T14:38:12.422Z",
        "updatedAt": "2019-04-24T14:38:12.422Z",
        "__v": 0
    },
    {
        "place": [
            "Harrenhal",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Bolton",
            "House Karstark"
        ],
        "factionsTwo": [
            "House Lannister",
            "House Clegane"
        ],
        "commandersOne": [
            "King Robb Stark",
            "Lord Roose Bolton",
            "Lord Rickard Karstark"
        ],
        "commandersTwo": [
            "Ser Gregor Clegane"
        ],
        "forcesOne": [
            "~18,000 Northern horsemen and infantry."
        ],
        "forcesTwo": [
            "Large Lannister garrison"
        ],
        "casualties": [
            "199 Northmen and Rivermen prisoners massacred",
            "Ser Jaremy",
            "Mallister",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85afd",
        "name": "Liberation of Harrenhal",
        "slug": "Liberation_of_Harrenhal",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.423Z",
        "updatedAt": "2019-04-24T14:38:12.423Z",
        "__v": 0
    },
    {
        "place": [
            "Hardhome",
            " beyond the Wall"
        ],
        "factionsOne": [
            "The Living",
            "Free Folk",
            "Night's Watch",
            "House Baratheon of Dragonstone"
        ],
        "factionsTwo": [
            "The Dead",
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [
            "Lord Commander Jon Snow",
            "Chieftain Tormund",
            "Chieftess Karsi &",
            "Loboda &",
            "Chieftain Dim Dalba",
            "King Stannis Baratheon "
        ],
        "commandersTwo": [
            "The Night King",
            "White Walker",
            "lieutenant &"
        ],
        "forcesOne": [
            "The Night's WatchEddison TollettTodderDuncan Liddle~20 Night's Watch rangersFree Folk~50,000-100,000 wildlingsWun Weg Wun Dar WunRoyal Fleet"
        ],
        "forcesTwo": [
            "About 4 White",
            "Walkers",
            "Wight army"
        ],
        "casualties": [
            "Massive casualtiesAll but about 5,000 wildlings killedKarsiLoboda~10 Night's Watch rangers",
            "Minimal casualtiesHundreds of wights - any wight losses offset by tens of thousands of new wights created from the wildlings killed in the massacreWhite Walker",
            "Lieutenant"
        ],
        "_id": "5cc074d404e71a0010b85afe",
        "name": "Massacre at Hardhome",
        "slug": "Massacre_at_Hardhome",
        "conflict": "Conflict Beyond the Wall",
        "createdAt": "2019-04-24T14:38:12.423Z",
        "updatedAt": "2019-04-24T14:38:12.423Z",
        "__v": 0
    },
    {
        "place": [
            "Meereen",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "House TargaryenUnsulliedDaenerys's Khalasar",
            "Second Sons"
        ],
        "factionsTwo": [
            "Meereen"
        ],
        "commandersOne": [
            "Queen Daenerys",
            "Targaryen",
            "Ser Jorah Mormont",
            "Ser Barristan Selmy",
            "Commander Grey Worm",
            "Captain Daario",
            "Naharis"
        ],
        "commandersTwo": [
            "Great Masters",
            "Oznak zo Pahl &"
        ],
        "forcesOne": [
            "Over 8,000",
            "UnsulliedUnder 100 Dothraki2,000 Second SonsAstapori and",
            "Yunkish freedmenTens of thousands of Meereenese slavesIndependent",
            "MercenariesAsher ForresterMalcolm BranfieldBeskha"
        ],
        "forcesTwo": [
            "Meereen City",
            "Guard"
        ],
        "casualties": [
            "Minimal",
            "Oznak zo PahlSeveral Great",
            "Masters killed by",
            "uprising slaves163 Great Masters crucified"
        ],
        "_id": "5cc074d404e71a0010b85aff",
        "name": "Siege of Meereen",
        "slug": "Siege_of_Meereen",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.423Z",
        "updatedAt": "2019-04-24T14:38:12.423Z",
        "__v": 0
    },
    {
        "place": [
            "Mummer's Ford",
            " the Trident"
        ],
        "factionsOne": [
            "House Lannister",
            "House Clegane"
        ],
        "factionsTwo": [
            "Iron Throne",
            "detachment sent on orders of Lord Eddard Stark as Hand of the King to arrest and",
            "execute Ser Gregor Clegane for the crimes committed against the Riverlands smallfolk"
        ],
        "commandersOne": [
            "Lord Tywin Lannister",
            "Ser Gregor Clegane"
        ],
        "commandersTwo": [
            "Lord Eddard Stark, Hand of the King",
            "Lord Beric",
            "Dondarrion &"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Detachment of",
            "approximately 100 men-at-arms mostly made up of House Stark and House Baratheon",
            "guardsmen, but also including Thoros of Myr and Anguy"
        ],
        "casualties": [
            "~45 men",
            "60 menBeric Dondarrion",
            "killed; later",
            "resurrected"
        ],
        "_id": "5cc074d404e71a0010b85b00",
        "name": "Battle at the Mummer's Ford",
        "slug": "Battle_at_the_Mummer%27s_Ford",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.423Z",
        "updatedAt": "2019-04-24T14:38:12.423Z",
        "__v": 0
    },
    {
        "place": [
            "Craster's Keep",
            " the HauntedForest",
            " Beyond the Wall"
        ],
        "factionsOne": [
            "Night's Watch",
            "Craster"
        ],
        "factionsTwo": [
            "Mutineers"
        ],
        "commandersOne": [
            "Lord Commander Jeor Mormont &",
            "Craster &"
        ],
        "commandersTwo": [
            "Karl Tanner"
        ],
        "forcesOne": [
            "Grenn",
            "Eddison Tollett",
            "Samwell Tarly",
            "Other black brothers"
        ],
        "forcesTwo": [
            "Over a dozen",
            "mutinous black",
            "brothersRast"
        ],
        "casualties": [
            "Craster",
            "Lord Commander Mormont",
            "Many loyalist black brothers",
            "Unknown, heavy"
        ],
        "_id": "5cc074d404e71a0010b85b01",
        "name": "Mutiny at Craster's Keep",
        "slug": "Mutiny_at_Craster%27s_Keep",
        "conflict": "Conflict Beyond the Wall",
        "createdAt": "2019-04-24T14:38:12.424Z",
        "updatedAt": "2019-04-24T14:38:12.424Z",
        "__v": 0
    },
    {
        "place": [
            "Sunspear"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "Dorne"
        ],
        "commandersOne": [
            "King Aegon I",
            "Targaryen",
            "Queen Visenya",
            "Targaryen",
            "Queen Rhaenys",
            "Targaryen"
        ],
        "commandersTwo": [
            "Princess Meria Martell"
        ],
        "forcesOne": [
            "Targaryen army",
            "3 dragonsBalerionVhagarMeraxes"
        ],
        "forcesTwo": [
            "Dornish army"
        ],
        "casualties": [
            "None",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b02",
        "name": "Occupation of Sunspear",
        "slug": "Occupation_of_Sunspear",
        "conflict": "First Dornish War",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.424Z",
        "updatedAt": "2019-04-24T14:38:12.424Z",
        "__v": 0
    },
    {
        "place": [
            "Old Wyk",
            " the the Iron Islands"
        ],
        "factionsOne": [
            "Iron Throne",
            " "
        ],
        "factionsTwo": [
            "Salt Throne"
        ],
        "commandersOne": [
            "Lord Commander",
            "Barristan Selmy"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85b03",
        "name": "Siege of Old Wyk",
        "slug": "Siege_of_Old_Wyk",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.424Z",
        "updatedAt": "2019-04-24T14:38:12.424Z",
        "__v": 0
    },
    {
        "place": [
            "Oxcross",
            " the Westerlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Bolton",
            "House Karstark"
        ],
        "factionsTwo": [
            "House Lannister"
        ],
        "commandersOne": [
            "King Robb Stark",
            "Lord Roose Bolton",
            "Lord Rickard Karstark"
        ],
        "commandersTwo": [
            "Ser Stafford",
            "Lannister &"
        ],
        "forcesOne": [
            "20,000, including one Direwolf "
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "~400 dead, wounded or imprisoned.",
            "At least 2,000 deadMany captured"
        ],
        "_id": "5cc074d404e71a0010b85b04",
        "name": "Battle of Oxcross",
        "slug": "Battle_of_Oxcross",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.424Z",
        "updatedAt": "2019-04-24T14:38:12.424Z",
        "__v": 0
    },
    {
        "place": [
            "Qohor",
            " the Free Cities"
        ],
        "factionsOne": [
            "Dothraki"
        ],
        "factionsTwo": [
            "Free City of Qohor"
        ],
        "commandersOne": [
            "Khal Temmo &"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "20,000"
        ],
        "forcesTwo": [
            "Unknown number of Qohorik city guard and heavy cavalrySecond SonsOne other",
            "mercenary company3,000 Unsullied"
        ],
        "casualties": [
            "12,000",
            "2,400 Unsullied,",
            "significant in the rest of the army. Both",
            "mercenary companies fled."
        ],
        "_id": "5cc074d404e71a0010b85b05",
        "name": "Battle of Qohor",
        "slug": "Battle_of_Qohor",
        "conflict": "Century of Blood",
        "createdAt": "2019-04-24T14:38:12.425Z",
        "updatedAt": "2019-04-24T14:38:12.425Z",
        "__v": 0
    },
    {
        "place": [
            "Lannisport",
            " the Westerlands"
        ],
        "factionsOne": [
            "House Greyjoy"
        ],
        "factionsTwo": [
            "House Lannister"
        ],
        "commandersOne": [
            "Prince Euron Greyjoy",
            "Prince Aeron Greyjoy"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Iron Fleet"
        ],
        "forcesTwo": [
            "Lannister Fleet"
        ],
        "casualties": [
            "Light",
            "Lannister Fleet burned at anchor"
        ],
        "_id": "5cc074d404e71a0010b85b06",
        "name": "Raid on Lannisport",
        "slug": "Raid_on_Lannisport",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.425Z",
        "updatedAt": "2019-04-24T14:38:12.425Z",
        "__v": 0
    },
    {
        "place": [
            "Sea Dragon Point",
            " The North"
        ],
        "factionsOne": [
            "House Forrester"
        ],
        "factionsTwo": [
            "Salt Throne"
        ],
        "commandersOne": [
            "Ser Royland Degore"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "The crews of a dozen Greyjoy longships"
        ],
        "_id": "5cc074d404e71a0010b85b07",
        "name": "Raid on Sea Dragon Point",
        "slug": "Raid_on_Sea_Dragon_Point",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.425Z",
        "updatedAt": "2019-04-24T14:38:12.425Z",
        "__v": 0
    },
    {
        "place": [
            "Raventree Hall",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Umber",
            "House Blackwood"
        ],
        "factionsTwo": [
            "House Lannister"
        ],
        "commandersOne": [
            "Lord Greatjon Umber"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "1500 Blackwood Men400 Umber Axemen"
        ],
        "forcesTwo": [
            "400 Lannister Garrison"
        ],
        "casualties": [
            "~70 Killed",
            "Entire garrison killed, wounded, or captured"
        ],
        "_id": "5cc074d404e71a0010b85b08",
        "name": "Liberation of Raventree Hall",
        "slug": "Liberation_of_Raventree_Hall",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.425Z",
        "updatedAt": "2019-04-24T14:38:12.425Z",
        "__v": 0
    },
    {
        "place": [
            "Tower of Joy",
            " Red Mountains",
            " Dorne"
        ],
        "factionsOne": [
            "House Stark",
            "House Reed",
            "House Glover",
            "House CasselHouse DustinHouse Ryswell"
        ],
        "factionsTwo": [
            "Kingsguard"
        ],
        "commandersOne": [
            "Lord Eddard Stark",
            "Lord Howland Reed "
        ],
        "commandersTwo": [
            "Lord Commander Gerold Hightower &"
        ],
        "forcesOne": [
            "6 NorthmenLord Eddard StarkLord Howland Reed "
        ],
        "forcesTwo": [
            "2 Kingsguard &Lord Commander",
            "Gerold Hightower &Ser Arthur Dayne &"
        ],
        "casualties": [
            "4 Northmen killed",
            "Lord Howland Reed wounded",
            "Both"
        ],
        "_id": "5cc074d404e71a0010b85b09",
        "name": "Showdown at the Tower of Joy",
        "slug": "Showdown_at_the_Tower_of_Joy",
        "conflict": "Robert's Rebellion (\"Abduction\" of Lyanna Stark)",
        "createdAt": "2019-04-24T14:38:12.426Z",
        "updatedAt": "2019-04-24T14:38:12.426Z",
        "__v": 0
    },
    {
        "place": [
            "The Twins",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "Houses of the North ",
            "House Forrester",
            "House Manderly",
            "House TullyHouses of the Riverlands"
        ],
        "factionsTwo": [
            "House Frey",
            "House Bolton",
            "House Whitehill"
        ],
        "commandersOne": [
            "~ King Robb Stark & ~ Ser Brynden Tully"
        ],
        "commandersTwo": [
            "~ Lord Tywin Lannister",
            "~ Lord Roose Bolton",
            "~ Lord Walder Frey"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Nigh-complete",
            "annihilation, few",
            "survivors captured",
            "King Robb Stark &Queen Talisa",
            "Stark &Unborn child of Robb and Talisa &Lady Catelyn",
            "Stark &Grey Wind &Ser Wendel",
            "Manderly &Lord Gregor",
            "Forrester &Lord Edmure Tully Most, if not all men in the camps",
            "Light",
            "Lady Joyeuse",
            "Erenford &Various soldiers"
        ],
        "_id": "5cc074d404e71a0010b85b0a",
        "name": "Red Wedding",
        "slug": "Red_Wedding",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.426Z",
        "updatedAt": "2019-04-24T14:38:12.426Z",
        "__v": 0
    },
    {
        "place": [
            "The Silence",
            " Blackwater Bay"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "House Greyjoy"
        ],
        "commandersOne": [
            "King Euron Greyjoy"
        ],
        "commandersTwo": [
            "Prince Theon Greyjoy"
        ],
        "forcesOne": [
            "Ironborn sailors and warriors loyal to Euron"
        ],
        "forcesTwo": [
            "Ironborn sailors and warriors loyal to Yara and Theon"
        ],
        "casualties": [
            "Minimal",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b0b",
        "name": "Rescue of Yara Greyjoy",
        "slug": "Rescue_of_Yara_Greyjoy",
        "conflict": "Great War, Daenerys Targaryen's invasion of Westeros",
        "createdAt": "2019-04-24T14:38:12.426Z",
        "updatedAt": "2019-04-24T14:38:12.426Z",
        "__v": 0
    },
    {
        "place": [
            "Harrenhal"
        ],
        "factionsOne": [
            "Greens"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "Prince Aemond",
            "Targaryen",
            "Ser Criston Cole"
        ],
        "commandersTwo": [
            "Prince Daemon",
            "Targaryen"
        ],
        "forcesOne": [
            "Green Army",
            "One dragon"
        ],
        "forcesTwo": [
            "One dragon"
        ],
        "casualties": [
            "None",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b0c",
        "name": "Retaking of Harrenhal",
        "slug": "Retaking_of_Harrenhal",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.427Z",
        "updatedAt": "2019-04-24T14:38:12.427Z",
        "__v": 0
    },
    {
        "place": [
            "Riverrun"
        ],
        "factionsOne": [
            "House Lannister",
            "Unnamed Lannister Bannermen"
        ],
        "factionsTwo": [
            "House Tully",
            "Unnamed Tully",
            "Bannermen"
        ],
        "commandersOne": [
            "Ser Jaime Lannister"
        ],
        "commandersTwo": [
            "Ser Edmure Tully"
        ],
        "forcesOne": [
            "30,000 Lannister",
            "soldiers"
        ],
        "forcesTwo": [
            "Riverrun Garrison"
        ],
        "casualties": [
            "Unknown",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85b0d",
        "name": "Siege of Riverrun",
        "slug": "Siege_of_Riverrun",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.427Z",
        "updatedAt": "2019-04-24T14:38:12.427Z",
        "__v": 0
    },
    {
        "place": [
            "Highgarden"
        ],
        "factionsOne": [
            "Iron Throne",
            "House Tarly"
        ],
        "factionsTwo": [
            "House Tyrell &"
        ],
        "commandersOne": [
            "Ser Jaime Lannister",
            "Ser Bronn",
            "Lord Randyll Tarly",
            "Dickon Tarly"
        ],
        "commandersTwo": [
            "Lady Olenna Tyrell &"
        ],
        "forcesOne": [
            "~10,000 Lannister",
            "infantry",
            "Tarly Army"
        ],
        "forcesTwo": [
            "Highgarden Garrison"
        ],
        "casualties": [
            "Light",
            "Nearly absoluteOlenna Tyrell"
        ],
        "_id": "5cc074d404e71a0010b85b0e",
        "name": "Sack of Highgarden",
        "slug": "Sack_of_Highgarden",
        "conflict": "Daenerys Targaryen's invasion of Westeros",
        "createdAt": "2019-04-24T14:38:12.427Z",
        "updatedAt": "2019-04-24T14:38:12.427Z",
        "__v": 0
    },
    {
        "place": [
            "Lannisport"
        ],
        "factionsOne": [
            "Greens",
            "House Lannister"
        ],
        "factionsTwo": [
            "Blacks",
            "House Greyjoy"
        ],
        "commandersOne": [
            "Johanna Lannister"
        ],
        "commandersTwo": [
            "Lord Dalton Greyjoy"
        ],
        "forcesOne": [
            "Lannister Fleet &"
        ],
        "forcesTwo": [
            "Iron Fleet"
        ],
        "casualties": [
            "Massive",
            "Low"
        ],
        "_id": "5cc074d404e71a0010b85b0f",
        "name": "Sack of Lannisport",
        "slug": "Sack_of_Lannisport",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.427Z",
        "updatedAt": "2019-04-24T14:38:12.427Z",
        "__v": 0
    },
    {
        "place": [
            "Mole's Town",
            " the Gift"
        ],
        "factionsOne": [
            "Night's Watch"
        ],
        "factionsTwo": [
            "Free Folk"
        ],
        "commandersOne": [
            "None"
        ],
        "commandersTwo": [
            "Chieftain Tormund",
            "Magnar Styr of Thenn"
        ],
        "forcesOne": [
            "Black brothersKegs &Mully &Black Jack Bulwer &"
        ],
        "forcesTwo": [
            "Tormund's warbandYgritteStyr's warbandThenn Warg"
        ],
        "casualties": [
            "Massive",
            "Negligible"
        ],
        "_id": "5cc074d404e71a0010b85b10",
        "name": "Sack of Mole's Town",
        "slug": "Sack_of_Mole%27s_Town",
        "conflict": "Conflict Beyond the Wall",
        "createdAt": "2019-04-24T14:38:12.428Z",
        "updatedAt": "2019-04-24T14:38:12.428Z",
        "__v": 0
    },
    {
        "place": [
            "Qohor",
            " The Free Cities"
        ],
        "factionsOne": [
            "Golden Company"
        ],
        "factionsTwo": [
            "Qohorik"
        ],
        "commandersOne": [
            "Aegor Rivers"
        ],
        "commandersTwo": [
            "None"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Negilgible",
            "Massive"
        ],
        "_id": "5cc074d404e71a0010b85b11",
        "name": "Sack of Qohor",
        "slug": "Sack_of_Qohor",
        "conflict": "",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.428Z",
        "updatedAt": "2019-04-24T14:38:12.428Z",
        "__v": 0
    },
    {
        "place": [
            "Tumbleton"
        ],
        "factionsOne": [
            "Greens",
            "House Hightower",
            "Dragonseeds"
        ],
        "factionsTwo": [
            "Blacks",
            "House Dustin",
            "Dragonseeds"
        ],
        "commandersOne": [
            "Prince Aemond",
            "Targaryen"
        ],
        "commandersTwo": [
            "Queen Rhaenyra",
            "Targaryen"
        ],
        "forcesOne": [
            "Hightower army",
            "Hugh Hammer",
            "Ulf the White"
        ],
        "forcesTwo": [
            "Tumbleton garrison",
            "Hugh Hammer",
            "Ulf the White"
        ],
        "casualties": [
            "Moderate to low",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85b12",
        "name": "Sack of Tumbleton",
        "slug": "Sack_of_Tumbleton",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.428Z",
        "updatedAt": "2019-04-24T14:38:12.428Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell",
            " the North"
        ],
        "factionsOne": [
            "House Stark",
            "House Mormont",
            "House Hornwood",
            "House Mazin",
            "Free Folk",
            "House Arryn"
        ],
        "factionsTwo": [
            "House Bolton",
            "House Karstark",
            "House Umber"
        ],
        "commandersOne": [
            "Jon Snow",
            "Chieftain Tormund Giantsbane",
            "Lady Lyanna",
            "Mormont",
            "Ser Davos Seaworth",
            "Lord Petyr Baelish",
            "Lady Sansa Stark",
            "Lord Yohn Royce"
        ],
        "commandersTwo": [
            "Lord Ramsay",
            "Bolton",
            "Lord Harald Karstark",
            "Lord Smalljon",
            "Umber &",
            "Bolton General",
            "Bolton Captain"
        ],
        "forcesOne": [
            "Around 2,400 men",
            "2,000 Free Folk",
            "warriors",
            "200 Hornwood",
            "soldiers",
            "143 Mazin soldiers",
            "62 Mormont soldiers",
            "Wun Weg Wun Dar Wun",
            "Unknown number of mounted Knights of the Vale"
        ],
        "forcesTwo": [
            "Around 6,000 men",
            "Contributions by each house unknown. Though the majority of the force is likely made up of Bolton forces."
        ],
        "casualties": [
            "Massive Stark",
            "casualtiesPrince Rickon",
            "StarkWun Weg Wun Dar Wun+1,500 killed/wounded",
            "Minimal Vale",
            "casualties",
            "Total annihilation of the Northern army",
            "under House Bolton; few survivors capturedLord Ramsay",
            "BoltonLord Smalljon",
            "Umber"
        ],
        "_id": "5cc074d404e71a0010b85b13",
        "name": "User blog:Salociin/Battle of the Bastards (Independent)",
        "slug": "User_blog:Salociin/Battle_of_the_Bastards_(Independent)",
        "conflict": "Battle of the Bastards",
        "createdAt": "2019-04-24T14:38:12.428Z",
        "updatedAt": "2019-04-24T14:38:12.428Z",
        "__v": 0
    },
    {
        "place": [
            "Seagard",
            " the Riverlands"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "House Mallister"
        ],
        "commandersOne": [
            "Prince Rodrik",
            "Greyjoy &"
        ],
        "commandersTwo": [
            "Lord Jason",
            "Mallister"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "HeavyRodrik Greyjoy",
            "Unknown, presumably light"
        ],
        "_id": "5cc074d404e71a0010b85b14",
        "name": "Battle of Seagard",
        "slug": "Battle_of_Seagard",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.429Z",
        "updatedAt": "2019-04-24T14:38:12.429Z",
        "__v": 0
    },
    {
        "place": [
            "Deepwood Motte",
            " the North"
        ],
        "factionsOne": [
            "House Greyjoy"
        ],
        "factionsTwo": [
            "House Bolton",
            "House Glover"
        ],
        "commandersOne": [
            "Unknown"
        ],
        "commandersTwo": [
            "Lord Robett Glover"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "Complete",
            "Unknown"
        ],
        "_id": "5cc074d404e71a0010b85b15",
        "name": "Second Battle of Deepwood Motte",
        "slug": "Second_Battle_of_Deepwood_Motte",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.429Z",
        "updatedAt": "2019-04-24T14:38:12.429Z",
        "__v": 0
    },
    {
        "place": [
            "Meereen",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "House TargaryenUnsulliedDothraki",
            "Second Sons"
        ],
        "factionsTwo": [
            "Slaver AllianceAstaporYunkaiVolantis",
            "Sons of the Harpy"
        ],
        "commandersOne": [
            "Queen Daenerys I",
            "Targaryen",
            "Lord Tyrion",
            "Lannister",
            "Commander Grey Worm",
            "Captain Daario",
            "Naharis"
        ],
        "commandersTwo": [
            "Razdal mo",
            "Eraz",
            "Yezzan zo",
            "Qaggaz",
            "Belicho",
            "Paenymion"
        ],
        "forcesOne": [
            "Around 110,000 men",
            "100,000 Dothraki",
            "8,000 Unsullied",
            "2,000 Second Sons",
            "3 dragons"
        ],
        "forcesTwo": [
            "Around 200 ships",
            "Ghiscari siege",
            "engineers and",
            "soldiersTrebuchetsSons of the Harpy"
        ],
        "casualties": [
            "Moderate",
            "At least one ship",
            "destroyed",
            "Fleet captured",
            "Many Sons of the HarpyBelicho Paenymion Razdal mo Eraz"
        ],
        "_id": "5cc074d404e71a0010b85b16",
        "name": "Second Siege of Meereen",
        "slug": "Second_Siege_of_Meereen",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.429Z",
        "updatedAt": "2019-04-24T14:38:12.429Z",
        "__v": 0
    },
    {
        "place": [
            "Riverrun",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Tully"
        ],
        "factionsTwo": [
            "Iron Throne",
            "House Lannister",
            "House Frey"
        ],
        "commandersOne": [
            "Ser Brynden Tully &"
        ],
        "commandersTwo": [
            "Ser Jaime Lannister",
            "Ser Bronn of the Blackwater",
            "Lothar Frey",
            "Walder Rivers"
        ],
        "forcesOne": [
            "Around 400 men"
        ],
        "forcesTwo": [
            "Around 12,000 men4,000 soldiers of House Frey8,000 soldiers of House Lannister"
        ],
        "casualties": [
            "Garrison surrendered;",
            "Brynden Tully killed while resisting arrest",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b17",
        "name": "Second Siege of Riverrun",
        "slug": "Second_Siege_of_Riverrun",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.429Z",
        "updatedAt": "2019-04-24T14:38:12.429Z",
        "__v": 0
    },
    {
        "place": [
            "Pyke",
            " Iron Islands"
        ],
        "factionsOne": [
            "Iron Throne",
            "House Stark",
            "House Lannister"
        ],
        "factionsTwo": [
            "Salt Throne"
        ],
        "commandersOne": [
            "King Robert I Baratheon",
            "Lord Stannis Baratheon",
            "Lord Eddard Stark",
            "Lord Tywin Lannister",
            "Ser Jaime Lannister",
            "Ser Jorah Mormont"
        ],
        "commandersTwo": [
            "King Balon Greyjoy",
            "Captain Euron Greyjoy",
            "Prince Maron Greyjoy &"
        ],
        "forcesOne": [
            "Thousands ten times larger than the Greyjoy army"
        ],
        "forcesTwo": [
            "Thousands"
        ],
        "casualties": [
            "Light - Moderate",
            "Maron Greyjoy",
            "A large percentage of the Greyjoy Forces"
        ],
        "_id": "5cc074d404e71a0010b85b18",
        "name": "Siege of Pyke",
        "slug": "Siege_of_Pyke",
        "conflict": "Greyjoy Rebellion",
        "createdAt": "2019-04-24T14:38:12.430Z",
        "updatedAt": "2019-04-24T14:38:12.430Z",
        "__v": 0
    },
    {
        "place": [
            "Rook's Rest",
            " The Crownlands"
        ],
        "factionsOne": [
            "Greens",
            "Kingsguard"
        ],
        "factionsTwo": [
            "Blacks"
        ],
        "commandersOne": [
            "King Aegon II",
            "Targaryen ",
            "Prince Aemond",
            "Targaryen",
            "Ser Criston Cole"
        ],
        "commandersTwo": [
            "Queen Rhaenyra",
            "Targaryen",
            "Princess Rhaenys",
            "Targaryen &"
        ],
        "forcesOne": [
            "Greens Army",
            "Kingsguard",
            "Aegon II Targaryen",
            "Sunfyre",
            "Aemond Targaryen",
            "Vhagar"
        ],
        "forcesTwo": [
            "Rhaenys Targaryen &",
            "Meleys &"
        ],
        "casualties": [
            "Most of the Greens ArmyAegon II Targaryen and Sunfyre wounded",
            "Rhaenys TargaryenMeleys"
        ],
        "_id": "5cc074d404e71a0010b85b19",
        "name": "Siege of Rook's Rest",
        "slug": "Siege_of_Rook%27s_Rest",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.430Z",
        "updatedAt": "2019-04-24T14:38:12.430Z",
        "__v": 0
    },
    {
        "place": [
            "Great Sept of Baelor",
            "King's Landing"
        ],
        "factionsOne": [
            "House Tyrell",
            "House Lannister"
        ],
        "factionsTwo": [
            "Faith Militant",
            "Iron Throne",
            "Kingsguard"
        ],
        "commandersOne": [
            "Lord Mace Tyrell",
            "Olenna Tyrell",
            "Lord Commander Jaime Lannister"
        ],
        "commandersTwo": [
            "The High Septon",
            "King Tommen I Baratheon"
        ],
        "forcesOne": [
            "Small company of House Tyrell"
        ],
        "forcesTwo": [
            "Many dozens",
            "of SparrowsBrother Lancel"
        ],
        "casualties": [
            "None",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b1a",
        "name": "Standoff at the Great Sept of Baelor",
        "slug": "Standoff_at_the_Great_Sept_of_Baelor",
        "conflict": "Rise and Fall of the Sparrows",
        "createdAt": "2019-04-24T14:38:12.430Z",
        "updatedAt": "2019-04-24T14:38:12.430Z",
        "__v": 0
    },
    {
        "place": [
            "Stone Hedge",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Umber"
        ],
        "factionsTwo": [
            "House Lannister"
        ],
        "commandersOne": [
            "Lord Greatjon Umber"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "600 Brackens300 Umber Spearmen"
        ],
        "forcesTwo": [
            "400 Lannister Garrison"
        ],
        "casualties": [
            "~50 Killed",
            "Entire garrison killed, wounded, or captured"
        ],
        "_id": "5cc074d404e71a0010b85b1b",
        "name": "Liberation of Stone Hedge",
        "slug": "Liberation_of_Stone_Hedge",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.430Z",
        "updatedAt": "2019-04-24T14:38:12.430Z",
        "__v": 0
    },
    {
        "place": [
            "Stone Mill",
            " the Riverlands"
        ],
        "factionsOne": [
            "House Tully",
            "House Mallister"
        ],
        "factionsTwo": [
            "House Lannister",
            "House Clegane"
        ],
        "commandersOne": [
            "Lord Edmure Tully",
            "Lord Jason Mallister"
        ],
        "commandersTwo": [
            "Ser Gregor Clegane"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "208 men",
            "400 DeadMartyn and Willem Lannister captured"
        ],
        "_id": "5cc074d404e71a0010b85b1c",
        "name": "Battle of Stone Mill",
        "slug": "Battle_of_Stone_Mill",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.431Z",
        "updatedAt": "2019-04-24T14:38:12.431Z",
        "__v": 0
    },
    {
        "place": [
            "Storm's End",
            " The Stormlands."
        ],
        "factionsOne": [
            "House Tyrell",
            "Land siege",
            "House Redwyne",
            "Naval blockade"
        ],
        "factionsTwo": [
            "House Baratheon",
            "Garrison",
            "House Stark",
            "Relief Force"
        ],
        "commandersOne": [
            "Lord Mace",
            "Tyrell",
            "Lord Paxter",
            "Redwyne"
        ],
        "commandersTwo": [
            "Ser Stannis Baratheon",
            "Lord Eddard Stark"
        ],
        "forcesOne": [
            "~40-50,000 soldiers",
            "Most of House Tyrell's army",
            "Redwyne Fleet"
        ],
        "forcesTwo": [
            "500 soldiers",
            "Storm's End Garrison",
            "Smuggler Davos",
            "Unknown Relief Force"
        ],
        "casualties": [
            "Minimal",
            "SignificantAll the animals at Storm's End"
        ],
        "_id": "5cc074d404e71a0010b85b1d",
        "name": "Siege of Storm's End",
        "slug": "Siege_of_Storm%27s_End",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.431Z",
        "updatedAt": "2019-04-24T14:38:12.431Z",
        "__v": 0
    },
    {
        "place": [
            "Dragonpit",
            " King's Landing"
        ],
        "factionsOne": [
            "Blacks",
            "City Watch"
        ],
        "factionsTwo": [
            "Smallfolk of King's Landing"
        ],
        "commandersOne": [
            "Queen Rhaenyra",
            "Targaryen",
            "Prince Joffrey",
            "Velaryon &"
        ],
        "commandersTwo": [
            "The Shepherd"
        ],
        "forcesOne": [
            "5 DragonsCity Watch guards"
        ],
        "forcesTwo": [
            "Thousands of",
            "SmallfolkHobb the Hewer"
        ],
        "casualties": [
            "Heavy5 Dragons killedPrince Joffrey",
            "Velaryon",
            "HeavyMany thousands of rioters"
        ],
        "_id": "5cc074d404e71a0010b85b1e",
        "name": "Storming of the Dragonpit",
        "slug": "Storming_of_the_Dragonpit",
        "conflict": "Dance of the Dragons",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.431Z",
        "updatedAt": "2019-04-24T14:38:12.431Z",
        "__v": 0
    },
    {
        "place": [
            "Summerhall",
            " The Stormlands"
        ],
        "factionsOne": [
            "House Baratheon"
        ],
        "factionsTwo": [
            "In quick succession",
            "3 House Baratheon Bannermen",
            "remaining loyal to the Iron Throne:House GrandisonHouse FellHouse Cafferen",
            "Elements of House Tarly"
        ],
        "commandersOne": [
            "Lord Robert Baratheon"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "Unknown"
        ],
        "forcesTwo": [
            "Unknown"
        ],
        "casualties": [
            "Unknown",
            "HeavyAt least one man of House Tarly"
        ],
        "_id": "5cc074d404e71a0010b85b1f",
        "name": "Battle of Summerhall",
        "slug": "Battle_of_Summerhall",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.432Z",
        "updatedAt": "2019-04-24T14:38:12.432Z",
        "__v": 0
    },
    {
        "place": [
            "Torrhen's Square",
            " the North"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "The North",
            "House Tallhart"
        ],
        "commandersOne": [
            "First Mate Dagmer"
        ],
        "commandersTwo": [
            "Ser Rodrik Cassel "
        ],
        "forcesOne": [
            "Crew of the Sea Bitch"
        ],
        "forcesTwo": [
            "Torrhen's Square",
            "garrison",
            "200 men of Winterfell"
        ],
        "casualties": [
            "2 Killed",
            "Ser Rodrik Cassel",
            "captured"
        ],
        "_id": "5cc074d404e71a0010b85b20",
        "name": "Raid on Torrhen's Square",
        "slug": "Raid_on_Torrhen%27s_Square",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.432Z",
        "updatedAt": "2019-04-24T14:38:12.432Z",
        "__v": 0
    },
    {
        "place": [
            "The ford on The Trident River"
        ],
        "factionsOne": [
            "House Baratheon",
            "House Stark",
            "House Bolton",
            "House Forrester",
            "House Arryn",
            "House Tully",
            "House Frey",
            "Arrived after the",
            "battle was over"
        ],
        "factionsTwo": [
            "Iron Throne",
            "Kingsguard",
            "Crownlands",
            "Bannermen",
            "House Martell"
        ],
        "commandersOne": [
            "Lord Robert Baratheon",
            "Lord Eddard Stark",
            "Lord Jon Arryn",
            "Lord Hoster Tully",
            "Lord Roose Bolton",
            "Lord Thorren",
            "Forrester &",
            "Lord Walder Frey"
        ],
        "commandersTwo": [
            "Prince Rhaegar",
            "Targaryen &",
            "Prince Lewyn",
            "Martell &",
            "Ser Barristan",
            "Selmy",
            "Ser Jonothor",
            "Darry &"
        ],
        "forcesOne": [
            "About",
            "35,000 troops",
            "but battle-hardened"
        ],
        "forcesTwo": [
            "More than",
            "40,000 troops",
            "but inexperienced",
            "including",
            "10,000 Dornishmen"
        ],
        "casualties": [
            "ModerateLord Robert Baratheon wounded in actionLord Thorren",
            "Forrester &",
            "Entire army destroyedPrince Rhaegar",
            "Targaryen &Ser Barristan Selmy, wounded",
            "and capturedPrince Lewyn",
            "Martell &Ser Jonothor",
            "Darry &"
        ],
        "_id": "5cc074d404e71a0010b85b21",
        "name": "Battle of the Trident",
        "slug": "Battle_of_the_Trident",
        "conflict": "Robert's Rebellion",
        "createdAt": "2019-04-24T14:38:12.432Z",
        "updatedAt": "2019-04-24T14:38:12.432Z",
        "__v": 0
    },
    {
        "place": [
            "Meereen",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "House TargaryenUnsullied",
            "Second Sons"
        ],
        "factionsTwo": [
            "Sons of the Harpy"
        ],
        "commandersOne": [
            "Queen Daenerys",
            "Targaryen",
            "Tyrion Lannister",
            "Ser Jorah Mormont",
            "Ser Barristan",
            "Selmy &",
            "Captain Daario",
            "Naharis",
            "Commander Grey Worm ",
            "Hizdahr zo Loraq &"
        ],
        "commandersTwo": [
            "The Harpy"
        ],
        "forcesOne": [
            "8,000 Unsullied",
            "2,000 Second Sons",
            "Drogon"
        ],
        "forcesTwo": [
            "Sons of the Harpy"
        ],
        "casualties": [
            "Numerous Unsullied",
            "White Rat",
            "Ser Barristan SelmyHizdahr zo Loraq",
            "Numerous Sons of the Harpy"
        ],
        "_id": "5cc074d404e71a0010b85b22",
        "name": "Uprising in Meereen",
        "slug": "Uprising_in_Meereen",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.432Z",
        "updatedAt": "2019-04-24T14:38:12.432Z",
        "__v": 0
    },
    {
        "place": [
            "Sunspear"
        ],
        "factionsOne": [
            "Iron Throne"
        ],
        "factionsTwo": [
            "Dorne"
        ],
        "commandersOne": [
            "Lord Rosby"
        ],
        "commandersTwo": [
            "Princess Meria Martell"
        ],
        "forcesOne": [
            "Targaryen army"
        ],
        "forcesTwo": [
            "Dornish army"
        ],
        "casualties": [
            "Moderate to heavy",
            "Low to Moderate"
        ],
        "_id": "5cc074d404e71a0010b85b23",
        "name": "Uprising in Sunspear",
        "slug": "Uprising_in_Sunspear",
        "conflict": "First Dornish War",
        "dateOfBattle": null,
        "createdAt": "2019-04-24T14:38:12.433Z",
        "updatedAt": "2019-04-24T14:38:12.433Z",
        "__v": 0
    },
    {
        "place": [
            "Whispering Wood",
            " near Riverrun in the Riverlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Karstark",
            "House Umber",
            "House Frey"
        ],
        "factionsTwo": [
            "House Lannister"
        ],
        "commandersOne": [
            "Lord Robb Stark",
            "Lord Rickard Karstark",
            "Lord Jon Umber"
        ],
        "commandersTwo": [
            "Ser Jaime Lannister "
        ],
        "forcesOne": [
            "~21,600"
        ],
        "forcesTwo": [
            "30,000 at Riverrun, unknown at the battle"
        ],
        "casualties": [
            "Unknown, likely light to moderateHarrion Karstark",
            "Unknown, army broken and scatteredSer Jaime Lannister "
        ],
        "_id": "5cc074d404e71a0010b85b24",
        "name": "Battle of the Whispering Wood",
        "slug": "Battle_of_the_Whispering_Wood",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.433Z",
        "updatedAt": "2019-04-24T14:38:12.433Z",
        "__v": 0
    },
    {
        "place": [
            "Beyond the Wall",
            " Westeros"
        ],
        "factionsOne": [
            "The Living",
            "Kingdom of the North",
            "Night's Watch",
            "Free Folk",
            "Brotherhood Without Banners",
            "House Targaryen"
        ],
        "factionsTwo": [
            "The Dead",
            "White Walkers",
            "Army of the dead"
        ],
        "commandersOne": [
            "King Jon Snow",
            "Queen Daenerys I Targaryen"
        ],
        "commandersTwo": [
            "Night King"
        ],
        "forcesOne": [
            "Hunting party:King Jon SnowChieftain TormundSer Jorah MormontLord Beric",
            "DondarrionGendryThoros &Sandor Clegane1 Night's Watch Ranger &3 Free Folk warriors &2 House Stark soldiers &Extraction forces:Queen Daenerys I Targaryen3 Dragons; Drogon, Rhaegal and Viserion &Benjen Stark &"
        ],
        "forcesTwo": [
            "Army of the dead:Several White",
            "WalkersApproximately one hundred thousand wights"
        ],
        "casualties": [
            "HeavyThoros1 ranger3 wildlings2 Stark soldiersViserion",
            "Benjen Stark",
            "Moderate1 White WalkerThousands of wights"
        ],
        "_id": "5cc074d404e71a0010b85b25",
        "name": "Wight Hunt",
        "slug": "Wight_Hunt",
        "conflict": "Conflict Beyond the Wall / Daenerys Targaryen's invasion of Westeros / Great War",
        "createdAt": "2019-04-24T14:38:12.433Z",
        "updatedAt": "2019-04-24T14:38:12.433Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell",
            " the North"
        ],
        "factionsOne": [
            "Salt Throne"
        ],
        "factionsTwo": [
            "The North"
        ],
        "commandersOne": [
            "Prince Theon Greyjoy",
            "First Mate Dagmer"
        ],
        "commandersTwo": [
            "Prince Bran Stark",
            "Ser Rodrik Cassel"
        ],
        "forcesOne": [
            "Crew of the Sea BitchLorren"
        ],
        "forcesTwo": [
            "A skeleton defense force"
        ],
        "casualties": [
            "2 men",
            "Light",
            "Ser Rodrik Cassel",
            "executed"
        ],
        "_id": "5cc074d404e71a0010b85b26",
        "name": "Fall of Winterfell",
        "slug": "Fall_of_Winterfell",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.433Z",
        "updatedAt": "2019-04-24T14:38:12.433Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell"
        ],
        "factionsOne": [
            "House Bolton"
        ],
        "factionsTwo": [
            "House Baratheon of Dragonstone &"
        ],
        "commandersOne": [
            "Lord Roose Bolton",
            "Ramsay Bolton"
        ],
        "commandersTwo": [
            "King Stannis Baratheon &"
        ],
        "forcesOne": [
            "5,000",
            "Winterfell Garrison",
            "Over 2,000 Bolton cavalryBrienne of Tarth "
        ],
        "forcesTwo": [
            "1,300 infantry"
        ],
        "casualties": [
            "MinimalMyranda &Gordy &Simpson &",
            "CompleteKing Stannis Baratheon &"
        ],
        "_id": "5cc074d404e71a0010b85b27",
        "name": "Battle of Winterfell",
        "slug": "Battle_of_Winterfell",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.434Z",
        "updatedAt": "2019-04-24T14:38:12.434Z",
        "__v": 0
    },
    {
        "place": [
            "Winterfell",
            " the North"
        ],
        "factionsOne": [
            "House Greyjoy"
        ],
        "factionsTwo": [
            "House Bolton"
        ],
        "commandersOne": [
            "Prince Theon Greyjoy ",
            "First Mate Dagmer"
        ],
        "commandersTwo": [
            "Castellan Ramsay Snow"
        ],
        "forcesOne": [],
        "forcesTwo": [],
        "casualties": [
            "19 Ironborn flayed aliveDagmer Lorren Gelmarr Stygg Aggar UrzenWex Theon Greyjoy",
            "captured by Ramsay Snow",
            "None"
        ],
        "_id": "5cc074d404e71a0010b85b28",
        "name": "Sack of Winterfell",
        "slug": "Sack_of_Winterfell",
        "conflict": "Ironborn Invasion of the NorthWar of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.434Z",
        "updatedAt": "2019-04-24T14:38:12.434Z",
        "__v": 0
    },
    {
        "place": [
            "Yellow Fork",
            " the Westerlands"
        ],
        "factionsOne": [
            "House Stark",
            "House Bolton",
            "House Karstark"
        ],
        "factionsTwo": [
            "House Lannister",
            "House Westerling"
        ],
        "commandersOne": [
            "King Robb Stark",
            "Lord Roose Bolton",
            "Lord Rickard Karstark"
        ],
        "commandersTwo": [
            "Unknown"
        ],
        "forcesOne": [
            "~20,000 northermen"
        ],
        "forcesTwo": [
            "600"
        ],
        "casualties": [
            "~120 Killed",
            "400 KilledMany Captured"
        ],
        "_id": "5cc074d404e71a0010b85b29",
        "name": "Battle of the Yellow Fork",
        "slug": "Battle_of_the_Yellow_Fork",
        "conflict": "War of the Five Kings",
        "createdAt": "2019-04-24T14:38:12.434Z",
        "updatedAt": "2019-04-24T14:38:12.434Z",
        "__v": 0
    },
    {
        "place": [
            "Yunkai",
            " Slaver's Bay"
        ],
        "factionsOne": [
            "House Targaryen",
            "Unsullied",
            "Daenerys's Khalasar",
            "Second Sons"
        ],
        "factionsTwo": [
            "Yunkai",
            "Second Sons"
        ],
        "commandersOne": [
            "Queen Daenerys",
            "Targaryen",
            "Ser Barristan Selmy",
            "Ser Jorah Mormont",
            "Commander Grey Worm",
            "Captain Daario",
            "Naharis"
        ],
        "commandersTwo": [
            "Wise Masters",
            "Razdal mo Eraz",
            "Captain Mero &",
            "Captain Prendahl",
            "na Ghezn &",
            "Lieutenant Daario",
            "Naharis"
        ],
        "forcesOne": [
            "Over 8,000",
            "UnsulliedUnder 100 Dothraki2,000 Second Sons"
        ],
        "forcesTwo": [
            "Yunkish City",
            "Watch",
            "Yunkish Slave",
            "Soldiers",
            "2,000 Second Sons"
        ],
        "casualties": [
            "Minimal, if any",
            "Heavy"
        ],
        "_id": "5cc074d404e71a0010b85b2a",
        "name": "Battle of Yunkai",
        "slug": "Battle_of_Yunkai",
        "conflict": "Liberation of Slaver's Bay",
        "createdAt": "2019-04-24T14:38:12.434Z",
        "updatedAt": "2019-04-24T14:38:12.434Z",
        "__v": 0
    }
]